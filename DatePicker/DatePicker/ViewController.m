//
//  ViewController.m
//  DatePicker
//
//  Created by Sarada Adhikarla on 11/23/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - callback for show button press -

- (IBAction)onPressingShowButton:(UIButton *)sender {
    
    self.doneButton = [[UIButton alloc]initWithFrame:CGRectMake(270, 168, 50, 30)];
    [self.doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [self.doneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.doneButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
    [self.doneButton addTarget:NULL
                        action:@selector(onPressingDoneButton:)
                        forControlEvents:UIControlEventTouchUpInside];
    self.doneButton.hidden = NO;
    [self.view addSubview:self.doneButton];
    
    self.datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0,200,0,0)];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    //self.datePicker.backgroundColor = [UIColor purpleColor];
    self.datePicker.hidden = NO;
    self.datePicker.date = [NSDate date];
    [self.view addSubview:self.datePicker];
    
}

- (void)onPressingDoneButton:(UIButton *)sender {
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateIntervalFormatterMediumStyle;
    [df setDateFormat:@"MM/dd/yyyy"];
    NSString *str=[NSString stringWithFormat:@"%@",[df  stringFromDate:self.datePicker.date]];
    //assign text to label
    self.dateText.text=str;
    
    [self.datePicker removeFromSuperview];
    [self.doneButton removeFromSuperview];
    
}
@end
