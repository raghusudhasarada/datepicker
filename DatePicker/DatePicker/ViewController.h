//
//  ViewController.h
//  DatePicker
//
//  Created by Sarada Adhikarla on 11/23/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)onPressingShowButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *dateText;
@property (strong) UIDatePicker *datePicker;
@property (strong) UIButton *doneButton;
- (void)onPressingDoneButton:(UIButton *)sender;
@end

